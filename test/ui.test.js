const puppeteer = require('puppeteer');
const url = 'http://localhost:3000';
const openBrowser = true;

const browserOptions = {headless: !openBrowser};
const screenshotsDir = 'test/screenshots/';

async function loadPage(callback) {
    const browser = await puppeteer.launch(browserOptions);
    const page = await browser.newPage();
    await page.goto(url);
    await callback(page, browser);
    await browser.close();
}

async function performLogin(page, username) {
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', username);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1');
    await page.keyboard.press('Enter');
    await page.waitForSelector(".swal2-success-ring");
    await page.waitFor(2000);
}

async function performSendMessage(page, message) {
    await page.type('#message-form > input[type=text]', message);
    await page.click('#message-form > button');
    await page.waitFor(500);
}

// 1 - example
test('[Content] Login page title should exist', async () => {
    await loadPage(async page => {
        let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
        expect(title).toBe('Login');
    });
});


// 2 
test('[Content] Login page join button should exist', async () => {
    await loadPage(async page => {
        let button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
        expect(button).toBe('Join');
    });
});

// 3 - example
test('[Screenshot] Login page', async () => {
    await loadPage(async page => {
        await page.screenshot({path: 'test/screenshots/login.png'});
    });
});

// 4
test('[Screenshot] Welcome message', async () => {
    await loadPage(async page => {
        await performLogin(page, 'John');
        await page.screenshot({path: screenshotsDir + "welcome-message.png"});
    });
});

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    await loadPage(async page => {
        await page.keyboard.press('Enter');
        await page.screenshot({path: screenshotsDir + "err_msg_when_login_without_user_and_room_name.png"});
    });
});

// 6
test('[Content] Welcome message', async () => {
    await loadPage(async page => {
        await performLogin(page, "John");

        let message = await page.$eval('.message__body > p', (content) => content.innerHTML);
        expect(message).toBe('Hi John, Welcome to the chat app');
    });
});

// 7 - example
test('[Behavior] Type user name', async () => {
    await loadPage(async page => {
        await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
        let userName = await page.evaluate(() => {
            return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
        });
        expect(userName).toBe('John');
    });
});

// 8
test('[Behavior] Type room name', async () => {
    await loadPage(async page => {
        await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', 200);

        let roomName = await page.evaluate(() => {
            return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
        });
        expect(roomName).toBe('R1');
    });

});

// 9 - example
test('[Behavior] Login', async () => {
    await loadPage(async page => {
        await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
        await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
        await page.keyboard.press('Enter', {delay: 100});

        await page.waitForSelector('#users > ol > li');
        let member = await page.evaluate(() => {
            return document.querySelector('#users > ol > li').innerHTML;
        });

        expect(member).toBe('John');
    });
});

// 10
test('[Behavior] Login 2 users', async () => {
    await loadPage(async page1 => {
        await performLogin(page1, 'John');
        await loadPage(async page2 => {
            await performLogin(page2, 'Mike');
            let member1 = await page2.evaluate(() => {
                return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
            });
            let member2 = await page2.evaluate(() => {
                return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
            });
            expect(member1).toBe('John');
            expect(member2).toBe('Mike');
        })
    });
});

// 11
test('[Content] The "Send" button should exist', async () => {
    await loadPage(async page => {
        await performLogin(page, 'John');

        let sendButton = await page.evaluate(() => {
            return document.querySelector('#message-form > button').innerHTML;
        });
        expect(sendButton).toBe('Send');
    });
});

// 12
test('[Behavior] Send a message', async () => {
    await loadPage(async page => {
        await performLogin(page, "John");
        await performSendMessage(page, 'sample message');

        let sender = await page.evaluate(() => {
            return document.querySelector('#messages > li:nth-child(2) > div:nth-child(1) > h4').innerHTML;
        });
        let message = await page.evaluate(() => {
            return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
        });
        expect(sender).toBe("John");
        expect(message).toBe("sample message");
    });


});

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    await loadPage(async page1 => {
        await performLogin(page1, 'John');
        await loadPage(async page2 => {
            await performLogin(page2, 'Mike');
            await performSendMessage(page1, 'Hi');
            await performSendMessage(page2, 'Hello');


            let message3Sender = await page1.evaluate(() => {
                return document.querySelector('#messages > li:nth-child(3) > div:nth-child(1) > h4').innerHTML;
            });
            let message3Text = await page1.evaluate(() => {
                return document.querySelector('#messages > li:nth-child(3) > div:nth-child(2) > p').innerHTML;
            });
            let message4Sender = await page1.evaluate(() => {
                return document.querySelector('#messages > li:nth-child(4) > div:nth-child(1) > h4').innerHTML;
            });
            let message4Text = await page1.evaluate(() => {
                return document.querySelector('#messages > li:nth-child(4) > div:nth-child(2) > p').innerHTML;
            });
            expect(message3Sender).toBe('John');
            expect(message3Text).toBe('Hi');
            expect(message4Sender).toBe('Mike');
            expect(message4Text).toBe('Hello');

            // test Mike's page
            message3Sender = await page2.evaluate(() => {
                return document.querySelector('#messages > li:nth-child(2) > div:nth-child(1) > h4').innerHTML;
            });
            message3Text = await page2.evaluate(() => {
                return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
            });
            message4Sender = await page2.evaluate(() => {
                return document.querySelector('#messages > li:nth-child(3) > div:nth-child(1) > h4').innerHTML;
            });
            message4Text = await page2.evaluate(() => {
                return document.querySelector('#messages > li:nth-child(3) > div:nth-child(2) > p').innerHTML;
            });
            expect(message3Sender).toBe('John');
            expect(message3Text).toBe('Hi');
            expect(message4Sender).toBe('Mike');
            expect(message4Text).toBe('Hello');
        });
    });
});

// 14
test('[Content] The "Send location" button should exist', async () => {
    await loadPage(async page => {
        await performLogin(page, 'John');
        let button = await page.evaluate(() => {
            return document.querySelector('#send-location').innerHTML;
        });
        expect(button).toBe('Send location');
    });
});

// 15
test('[Behavior] Send a location message', async () => {
    await loadPage(async page => {
    // mock API of getting current position
        await page.evaluateOnNewDocument(async function () {
            navigator.geolocation.getCurrentPosition = function (callback) {
                setTimeout(() => {
                    callback({
                        'coords': {
                            accuracy: 1649,
                            altitude: null,
                            altitudeAccuracy: null,
                            heading: null,
                            latitude: 25.021644799999997,
                            longitude: 121.5463424,
                            speed: null
                        }
                    })
                }, 100)
            }
        });

        await performLogin(page, "John");

        await page.click('#send-location');
        await page.waitFor(4000);

        // expect to sent a message of maps.google.com API which shows the map
        let messageBody = await page.evaluate(() => {
            return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > div > iframe').src;
        });
        const api = new URL(messageBody);
        expect(api.hostname).toBe('maps.google.com');
    });
});
